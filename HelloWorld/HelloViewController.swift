//
//  HelloViewController.swift
//  HelloWorld
//
//  Created by New Owner on 28/06/2016.
//  Copyright © 2016 gi. All rights reserved.
//

import UIKit

class HelloViewController: UIViewController {
    
    @IBOutlet weak var helloLabel: UILabel!
    @IBOutlet var hiButton: UIButton!
    @IBOutlet var byeButton: UIButton!
    
    //var drops: [UILabel] = []
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        //hiButton.addTarget(self, action: #selector(HelloViewController.sayHi), forControlEvents: UIControlEvents.TouchUpInside)
        let floodRect = CGRect(x: 0, y: self.view.frame.height, width: self.view.frame.width, height: self.view.frame.height)
        let flood : UIView = UIView(frame: floodRect)
        flood.backgroundColor = UIColor.blueColor()
        self.view.addSubview(flood)
        
        for _ in 0...500 {
            self.makeRainDrop(flood)
        }
    }
    
    override func viewDidAppear(animated: Bool) {
        
        
    }
    
    func makeRainDrop(flood: UIView) {
        
        let randX = CGFloat(arc4random_uniform(UInt32(self.view.frame.width))+1)%self.view.frame.width
        let rect = CGRect(x: randX, y: -self.view.frame.height - 60, width: 60, height: 60)
        let water: UILabel = UILabel(frame: rect)
        water.text = "💧"
        water.font.fontWithSize(CGFloat(arc4random_uniform(15)+8))
        let scaleRandom = CGFloat(arc4random_uniform(8)+2) / 4
        water.transform = CGAffineTransformScale(CGAffineTransformIdentity, scaleRandom, scaleRandom)
        self.view.addSubview(water)
        
        let randomDelay = Double (arc4random_uniform(100))/10
        let randomTime = Double(3 - scaleRandom)
        
        
        UIView.animateWithDuration(randomTime, delay: randomDelay, options: .CurveLinear, animations: {
            
            flood.frame.origin.y -= 1
            
            water.frame = CGRect(x: water.frame.origin.x,
                y:  flood.frame.origin.y,
                width: water.frame.width,
                height: water.frame.height
            )
            self.view.layoutIfNeeded()
            }, completion: { (Bool: Bool) -> Void in
                water.removeFromSuperview()
                if Bool && flood.frame.origin.y > 0 {
                    self.makeRainDrop(flood)
                } else {
                    print ("Flooded!")
                }
                })
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func sayHi(sender: AnyObject) {  
        //Added more comment
        if let button: UIButton = sender as? UIButton {
            
            UIView.animateWithDuration(0.5, animations: {
                
                if button == self.hiButton {
                    self.helloLabel.text = "😘"
                    self.helloLabel.transform = CGAffineTransformIdentity
                } else if button == self.byeButton {
                    self.helloLabel.text = "😄"
                    self.helloLabel.transform = CGAffineTransformRotate(CGAffineTransformIdentity, CGFloat(-180.0 * M_PI / 180.0))
                }
            })
        }
    
    }
    
    @IBAction func sayBye() {
        print("say bye now")
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
